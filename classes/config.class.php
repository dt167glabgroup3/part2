<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: config.class.php
 * Desc: Class Config for website
 *
 * Your name
 ******************************************************************************/
// $config = config::getInstance(); // load the configs

class Config {
    protected static $instance = null; // Instans of singelton

    // all private so they cant be changed by something
    private $websiteTitle,$debugMode,$PassMinLength,$PassMaxLength,$PassReqSmall,$PassReqBig,$PassReqSpecial;
    private $dbSettingsAdmin = array(); // array for database settings
    private $dbSettingsRead = array();  // array for database settings
    private $dbSettingsWrite = array(); // array for database settings


    //Constructor
    protected function __construct() { // protectec because of singleton
        include ('includes/config.php'); //load the config file
        // Resave vales from config file.
        $this->websiteTitle = $title;
        $this->debugMode = $debug;
        $this->PassMinLength = $PasswordMinLength;
        $this->PassMaxLength = $PasswordMaxLength;
        $this->PassReqSmall = $PasswordReqSmall;
        $this->PassReqBig = $PasswordReqBig;
        $this->PassReqSpecial = $PasswordReqSpecial;

        // arrays for login values
        // structure:
        // login name, login pass, server url, server port, database name, schematic name
        $this->dbSettingsAdmin = $DBuserAdmin;
        array_push($this->dbSettingsAdmin,$DBhost,$DBport,$DBName,$DBschem); 
        $this->dbSettingsWrite = $DBuserWrite;
        array_push($this->dbSettingsWrite,$DBhost,$DBport,$DBName,$DBschem);
        $this->dbSettingsRead = $DBuserRead;
        array_push($this->dbSettingsRead,$DBhost,$DBport,$DBName,$DBschem);       

        // run the debug setting
        if ($debug) {
            error_reporting(E_ALL);
            ini_set('display_errors', 'On');
            //echo "<h1>DEBUG MODE IS ON </h1>";
        }else{
            error_reporting(0);
            ini_set('display_errors', 'Off');
        }
    }

    protected function __clone()
    {}

    // stop it from being deleted
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }

    // To get the singleton instance
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }
    
    // Gets ---------------------------------

    // get database connection values
    public function getDBAdminConnect(){
        return $this->dbSettingsAdmin;
    }
    public function getDBReadConnect(){
        return $this->dbSettingsRead;
    }
    public function getDBWriteConnect(){
        return $this->dbSettingsWrite;
    }

    // get the sites title
    public function getTitle(){
        return $this->websiteTitle;
    }

    // Get Password settings
    // Note, on debug it will ignore all restrictions for password.
    public function passGetMin(){
        if($this->debugMode){
            return 0;
        }else{
            return $this->PassMinLength;
        }
    }
    public function passGetMax(){
        if($this->debugMode){
            return 999999;
        }else{
            return $this->PassMaxLength;
        }
    }
    public function passLowerNeeded(){
        if($this->debugMode){
            return false;
        }else{
            return $this->PassReqSmall;
        }
    }
    public function passSpecialNeeded(){
        if($this->debugMode){
            return false;
        }else{
            return $this->PassReqSpecial;
        }
    }
    public function passUpperNeeded(){
        if($this->debugMode){
            return false;
        }else{
            return $this->PassReqBig;
        }
    }
    
    // Sets ------------------------------------------------
    // ------! should be no sets for this class !----
    
    
    
}