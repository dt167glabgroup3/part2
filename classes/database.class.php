<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: databas.class.php
 * Desc: Parent for database classes, this virutal and cant be used for connections.
 *
 * Public functions.
 * getInstance()
 * close()
 * countRow($table, $whereArr = false, $isArr = false)
 * getRow($table, $whereArr = false, $isArr = false, $select = "*", $limit = 0, $orderby = false, $order = "asc")
 * insertRow($table, $nameArr, $valueArr)
 * deleteRow($table, $whereArr, $isArr)
 *
 * Erho0903
 ******************************************************************************/

abstract class Database {

	protected static $instance = null; // Instans of singelton
    protected $conn = null;
    // Constructs are private to ensure Singleton
    protected function __construct()
    { }

    protected function __clone()
    {}

    // stop it from being deleted
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }

    // To get the singleton instance
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static;
            /*
            if(!static::$instance->connect()){
                exit(); // connection error, kill the site for safty
                return false;
            }*/
        }
        return static::$instance;
    }


    // Connect to the database
    // this virtual one cant connect
    abstract protected function connect();

    // connection will be closed, used in footer
    public function close() {
        if($this->$conn){
            mysqli_close($this->conn);
        }
    }


    /**
    * Count rows, takes arrays or strings as argument
    * returns 0 if miss or 0 in count
    * countRows("user","id","1")
    */
    public function countRow($table, $whereArr = false, $isArr = false) {
        $whereString = $this->getWhere($whereArr, $isArr);
        $result = $this->conn->query(sprintf("SELECT COUNT(*) FROM %s%s", $table, $whereString));
        if($result){
            return ($result->fetch_row())[0];
        }else{
            return 0;
        }
        $this->close();
    }

    /**
     * Get all rows from a table depending on query.
     * Takes arguments as void, string or array.
     * limit 0 = unlimited.
     ** Examples
     * getRows("table");
     * This will grab all from table "table".
     * getRows("table", array("name","phone"), array("eric","1234"), "name", 1, "name", "desc");
     * This will get name on table with where name eric and phone 1234, limit to 1, order by the name descending.
     */
    public function getRow($table, $whereArr = false, $isArr = false, $select = "*", $limit = 0, $orderby = false, $order = "asc") {
        $this->connect();
        $whereString = $this->getWhere($whereArr, $isArr);
        $ending = "";

        if($orderby){
            $ending .= " ORDER BY ".$orderby." ".$order;
        }
        if($limit != 0 && is_int($limit)){
            $ending .= " LIMIT ".$limit;
        }

        $result = $this->conn->query(sprintf("SELECT %s FROM %s%s %s",$select, $table, $whereString, $ending));
        if($result){
            return mysqli_fetch_all($result,MYSQLI_ASSOC);
        }else{
            return false;
        }

    }
		
    /**
     * Inserting to a row, takes table and arrays or strings as args
     * Value is escaped by escape_string
     ** Examples
     *  instertRow("table",array("name","phone"),array("eric","1234"));
     *  instertRow("table", "name", "eric");
     */
    public function insertRow($table, $nameArr, $valueArr) {

        $names = "";
        $values = "";
        // check that there are values
        if(is_array($valueArr)&&is_array($nameArr)){
            if(count($valueArr) != count($nameArr) && count($nameArr) > 0){
                return false;
            }else{
                // Two arrays, add values to sql strings
                // loop the arrays to make the sql string.
                $first = true;
                foreach ($nameArr as $val) {
                    if(!$first){
                        $names .= ',';
                    }else{
                        $first = false;
                    }
                    $names .= $val;
                }
                $first = true;
                foreach ($valueArr as $val) {
                    if(!$first){
                        $values .= ',';
                    }else{
                        $first = false;
                    }
                    $values .= "'".mysqli_real_escape_string($this->conn,$val)."'";
                }
            }
        }elseif(!is_array($valueArr)&&!is_array($nameArr)){
            // two strings, add avalues to sql strings
            $names = $nameArr;
            $values = "'".mysqli_real_escape_string($this->conn,$valueArr)."'";
        }else{
            return false; // one array and one string
        }

        // Run the query
        if(strlen($names) > 2 && strlen($values) > 2){
            $result = $this->conn->query(sprintf('INSERT INTO %s (%s) VALUES (%s)',$table,$names,$values));
            if ($result) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    /**
     * Delete a row from the table.
     * deleteRows("table","id","1");
     */
    public function deleteRow($table, $whereArr, $isArr) {

        $whereString = $this->getWhere($whereArr, $isArr);
        if($whereString){
            $newResult = $this->conn->query(sprintf("DELETE FROM %s%s", $table, $whereString));
            if ($newResult) {
                return true;
            }else{
                return false;
            }
        }else{
            // delete cant be run with empty where and is arguments!
            return false;
        }

    }

     /**
     * Update row.
     */
    public function updateRow($table, $whereArr, $isArr, $setWhereArr, $setIsArr) {

        $whereString = $this->getWhere($whereArr, $isArr);
        $whatString = $this->getWhat($setWhereArr, $setIsArr);
        if($whereString){
            $newResult = $this->conn->query(sprintf('UPDATE %s SET %s %s',$table,$whatString,$whereString));
            if ($newResult) {
                return true;
            }else{
                return false;
            }
        }else{
            // delete cant be run with empty where and is arguments!
            return false;
        }
    }

    // This function cleares the where and is arrays, all is get cleared by escape_string.
    protected function getWhere($whereArr, $isArr) {
        $where = "";
        // check that there are values
        if(!$whereArr || !$isArr){
            return "";
        }
        elseif(is_array($whereArr)&&is_array($isArr)){ // if arrays
            if(count($whereArr) != count($isArr) && count($isArr) > 0){
                return "";
            }else{
                // Two arrays, add values to sql strings
                // loop the arrays to make the sql string.
                $first = true;
                for($i=0;$i < count($whereArr);$i++) {
                    if(!$first){
                        $where .= ' AND ';
                    }else{
                        $where = ' WHERE ';
                        $first = false;
                    }
                    $where .= $whereArr[$i]. "='".mysqli_real_escape_string($this->conn,$isArr[$i])."'";
                }
            }
        }elseif(!is_array($whereArr)&&!is_array($isArr)){ // if strings
            // two strings, add avalues to sql strings
            $where = " WHERE ".$whereArr. "='".mysqli_real_escape_string($this->conn,$isArr)."'";
        }else{
            // string and array, nope
            return "";
        }
        return $where;
    }

    protected function getWhat($whereArr, $isArr){
        $what = "";
        if(!$whereArr || !$isArr){
            return "";
        }
        elseif(is_array($whereArr)&&is_array($isArr)){ // if arrays
            if(count($whereArr) != count($isArr) && count($isArr) > 0){
                return ""; // same lenght
            }else{
                // Two arrays, add values to sql strings
                // loop the arrays to make the sql string.
                $first = true;
                for($i=0;$i < count($whereArr);$i++) {
                    if(!$first){
                        $what .= ', ';
                    }else{
                        $first = false;
                    }
                    $what .= $whereArr[$i]. "='".mysqli_real_escape_string($this->conn,$isArr[$i])."'";
                }
            }
        }elseif(!is_array($whereArr)&&!is_array($isArr)){ // if strings
            // two strings, add avalues to sql strings
            $what = $whereArr. "='".mysqli_real_escape_string($this->conn,$isArr)."'";
        }else{
            // string and array, nope
            return "";
        }
        return $what;
    }
}
?>
