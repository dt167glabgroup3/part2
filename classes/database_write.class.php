<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: database_write.class.php
 * Desc: Class Database for writing to common tables.
 *
 * Public functions.
 * getInstance()
 * close()
 * countRow($table, $whereArr = false, $isArr = false)
 * getRow($table, $whereArr = false, $isArr = false, $select = "*", $limit = 0, $orderby = false, $order = "asc")
 * insertRow($table, $nameArr, $valueArr)
 * deleteRow($table, $whereArr, $isArr)
 *
 * Erho0903
 ******************************************************************************/

class Database_write extends Database {
    private static $instance1;

    public static function getInstance()
    {
        if (!isset(static::$instance1)) {
            static::$instance1 = new static;
            
            if(!static::$instance1->connect()){
                exit(); // connection error, kill the site for safty
                return false;
            }
        }
        return static::$instance1;
    }

	// Override
	protected function connect() {
		$settings = config::getInstance();

		// Connecting with an account that has limited access, only for writing to some tables
		$connectArray = $settings->getDBWriteConnect();
		
		$this->conn = new mysqli($connectArray[2], $connectArray[0], $connectArray[1], $connectArray[4]); //connecting to database

        if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			return false;
		}	
		$this->conn->set_charset("utf8");

		return true;
    }

    // Write is not allowed to count
    public function countRow($table, $whereArr = false, $isArr = false) {
    	return false;
    }

    // Write is not allowed get rows
    public function getRow($table, $whereArr = false, $isArr = false, $select = "*", $limit = 0, $orderby = false, $order = "asc") {
    	return false;
    }
    
    // Only admin can delete
    public function deleteRow($table, $whereArr, $isArr) {
    	return false;
    }

     /** // only admin
     * Update row.
     */
    public function updateRow($table, $whereArr, $isArr, $setWhereArr, $setIsArr) {
        return false;
    }
}
?>