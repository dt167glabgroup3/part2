<?PHP
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: config.php
 * Desc: Config values, used only by static config class
 *
 * Your name here
 ******************************************************************************/

/* Default settings */
$title = "Secure Messages"; // Site title and headliner
$debug = false; // If using debug mode, change before live (true / false)

/* Other settings */
date_default_timezone_set("Europe/Stockholm"); // if the servers timezone is wrong


/* Database settings */

// login to the Postgres server
$DBhost = "localhost"; // db url, maybe studentpsql.miun.se?
$DBport = 80; // database port
$DBName = "securemessages"; // database name
$DBschem = "notUsedCurrently"; // scema on the database

// accounts used on database
$DBuserRead = array("root",""); // user for read
$DBuserWrite = array("root",""); // user for write
$DBuserAdmin = array("root",""); // user for sensitive things, password hashes and deletes etc

/* Password settings */

$PasswordMinLength = 8;
$PasswordMaxLength = 70;
$PasswordReqSmall = true;
$PasswordReqBig = true;
$PasswordReqSpecial = false;

?>
