<?php
/*******************************************************************************
 * Projekt, Kurs: DT161G
 * File: start.php
 * Desc: File loaded at the start of the website
 *
 * erho0903
 ******************************************************************************/
session_name("securemessage-site");
session_start();
session_regenerate_id(); // refresh ID

include "includes/util.php";

// autoload classes, taken from older assignment
function my_autoloader($class) {
    $classfilename = strtolower($class);
    include 'classes/' . $classfilename . '.class.php';
}
spl_autoload_register('my_autoloader');


//Session destroy on timer due inactivity
if(isset($_SESSION["user_id"])){
	if(isset($_SESSION['timestamp'])){
		if((time() - $_SESSION['timestamp']) > 300) { //session max 300(5min)
			killItWithFire();//destroy session
			header("Location: index.php");
			die();
		} 
		else{
			$_SESSION['timestamp'] = time(); //set new timestamp
		}
	}
	else{//If you are here you got no timestamp, very bad.
		killItWithFire();
		die();
	}
}
	
/* 	Form listeners for all pages
*	They check that form values are correct.
* 	Will call functions in util.php
*   Note: they never contact the DB, that job is for functions only.
*/

// login and out
if(isset($_POST['loginSubmit'])){
	if(empty($_POST['username'])){
		$loginError = "No Username";
	}
	elseif(empty($_POST['password'])){
		$loginError = "No Password";
	}else{
        $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $username = trim($username);
		$loginError = login($username, $_POST['password']);
	}	
}

if(isset($_POST['logoutButton'])){
	logout();
}



?>

