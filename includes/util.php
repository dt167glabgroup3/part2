<?php
/*******************************************************************************
 * Projekt, Kurs: DT16/G
 * File: util.php
 * Desc: Util file for Projekt
 *
 * erho0903
 ******************************************************************************/

// GENERAL FUNCTIONS HERE -----------------------------------------------------------------

function login($name, $pass){
    $db = Database_read::getInstance();

    if($db->countRow("member","name",$name) == 1){
    	$user = ($db->getRow("member","name",$name))[0];
    	$user_id = $user["id"];
    	$db_password = $user["password"];
        // If user exists, check login-status. You are only allowerd 5 login-attempts in 30 minutes.
        if(!validateLogin($user_id)){
            return "Account Locked out! Try again later.";
        }
        else{
            // Check password
            if(password_verify($pass,$db_password)){
                // If password is correct. Set session variables.
                $_SESSION["user"] = serialize($user);
                $_SESSION["user_id"] = $user_id;
                $_SESSION['timestamp'] = time();
                return false; // returning flase when everything is ok, else text of error
            }
            else{
                // If the password is wrong, insert the user-id and login-time in the database.
                logfile("wrongpassword",$user_id);
                return "Wrong username or password"; // Same error message as wrong name
            }
        }
    }
    else {
        // If the user doesn't exist in database!
        return "Wrong username or password"; // Same error message as wrong password
    }
}


// Check login attempts from the past 30 minutes
function validateLogin($user_id){
    $db = Database_read::getInstance();

    $badLogins = $db->getRow("logs",array("event","result"),array("wrongpassword",$user_id),"stamp",5,"stamp","DESC");

    // If Total bad logins are below 5
    if(count($badLogins) < 5){
    	return true;
    }else{
    	$badLogin = $badLogins[4]["stamp"]; // the 5th bad login, (2018-05-23 13:13:0)
    	$badTime = strtotime($badLogin);
    	$valid_logins = time() - (60*30);  // for 30 minutes (60*30)
    	if($badTime > $valid_logins){
    		return false;
    	}else{
    		return true;
    	}
    }
}


function logout(){
	// Unset all of the session variables.
	$_SESSION = array();
	if (ini_get("session.use_cookies")) {
	    $params = session_get_cookie_params();
	    setcookie(session_name(), '', time() - 42000,
	        $params["path"], $params["domain"],
	        $params["secure"], $params["httponly"]
	    );
	}

	// Destroy the session.
	session_destroy();

}

function addNewMember($name, $phone, $password)
{
  $db = database_admin::getInstance();
  if($db->insertRow("member", array("name", "phone" ,"password"), array($name, $phone , $password))){
    return false; // no errors
  }else{
    return "Database error.";
  }
}

function checkNewMember($name){
  $db = database_read::getInstance();
  $result = $db->getRow("member", "name", $name);

  if(!$result){ //not already exist
    return false;
  }
  else
  {
    return "Name already in use!";
  }

}

function getUserInfo($name)
{
  $db = database_read::getInstance();
  $result = $db->getRow("member", "name", $name);

  if($result){ //not already exist
    return $result;
  }
  else
  {
    return "Database error!";
  }
}
// returns false if valid, else text of whats wrong
// note: only one error will be found and returned, even if there are many
function checkPassword($pass){
	$settings = config::getInstance();

	if(strlen($pass) < $settings->passGetMax()){
		if(strlen($pass) >= $settings->passGetMin()){
			$commonPwArray = file("includes/Top10k-probable.txt");
			if(!in_array($pass, $commonPwArray)){
				if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-!.]/', $pass) || !$settings->passSpecialNeeded()){
					if (preg_match('/[abcdefghijklmnopqrstuvxyzåäö]/', $pass) || !$settings->passLowerNeeded()){
						if (preg_match('/[ABCDEFGHIJKLMNOPQRSTUVXYZÅÄÖ]/', $pass) || !$settings->passUpperNeeded()){
							return false; // password is ok
						}
					}
				}
			}
		}
	}

	// The password didn't pass the tests
	return "Password not saved. Check requirements.";
}

// log an event, maybe logfile("User creation","Successful creation");
function logfile($event,$value){
	$db = Database_write::getInstance();
	return $db->insertRow("logs",array("event","result"),array($event,$value));
}

function clean($var) {
	$var = strip_tags($var);
	$var = preg_replace('/[^A-Öa-ö0-9\. -]@/', '', $var);
	$var = preg_replace('/  */', ' ', $var);
	return htmlspecialchars($var);
}

// Functions for message system
function uploadMessage($message,$userId){
	$db = Database_write::getInstance();
	if(!($db->insertRow("messages",array("member","message"),array($userId,$message)))){
		return "Error uploading message";
	}else{
		return false; // can add a upladed ok message here
	}
}

function deleteMessage($messageId,$userId){
	$db = Database_admin::getInstance();
	$message = $db->getrow("messages","id",$messageId);
	if($message){
		$messageOwner = $message[0]["member"];
		if($messageOwner == $userId){
			return $db->deleteRow("messages","id",$messageId);
		}else{
			return false; // user not same as owner, dont delete.
		}
	}else{
		return false; // invalid ID
	}

}

function downvoteMessage($messageId,$userId){
	voteMessage($messageId,$userId,0);
}
function upvoteMessage($messageId,$userId){
	voteMessage($messageId,$userId,1);
}

function voteMessage($messageId,$userId,$vote){
	$db = Database_read::getInstance();
	$message = $db->getRow("messages","id",$messageId);
	if($message){ // check message exists
		if($message[0]["member"] != $userId){ // never vote on your own post
			if(0 == $db->countRow("vote",array("post","member"),array($messageId,$userId))){ // not voted on this one before
				$dbWrite = Database_write::getInstance();
				$dbWrite->insertRow("vote",array("member","post","type"),array($userId,$messageId,$vote));
			}
		}
	}

}

function getMessages($wordSearch,$memberSearch,$orderSearch,$firstSearch){
	$newMessageArray = array();
	$db = Database_read::getInstance();

	if($firstSearch == "asc"){
		$fallOrder = "ASC";
	}else{
		$fallOrder = "DESC";
	}

	if(!is_null($memberSearch)){
		$user = $db->getRow("member","name",$memberSearch,"id",1);
		if($user){
			$messages = $db->getRow("messages","member",$user[0]["id"],"*",999,"stamp",$fallOrder);
		}else{
			//user dont exist
			return array();
		}
	}
	else{
		$messages = $db->getRow("messages",false,false,"*",999,"stamp",$fallOrder);
		if(!$messages){
			return array(); // no hits, return empty array
		}
	}


    if($messages && count($messages)>0){

	    if(!is_null($wordSearch)){
	    	//filter on that word
			foreach ($messages as $key => $value){
				if(stripos($value["message"],$wordSearch) === false){
					unset($messages[$key]);
				}
			}
	    }

	    // Add the votes for each post to the array
		foreach ($messages as $key => $value){
			$positive = $db->countRow("vote",array("post","type"),array($value["id"],1));
			$negative = $db->countRow("vote",array("post","type"),array($value["id"],0));
			array_push($newMessageArray,array($value,$positive,$negative));
		}


		if($orderSearch == "score"){
	    	//sort on score
			usort($newMessageArray, function($a, $b) {
				return  ($b[1]-$b[2]) - ($a[1]-$a[2]);
	    	});
	    }

    	//flip if needed
    	if($firstSearch == "asc"){
    		$newMessageArray = array_reverse($newMessageArray);
    	}

    }


	return $newMessageArray;
}
function resetAvailable(){
	$db = Database_read::getInstance();

	$pass = false; // passing the test, start as false

	// If Total bad reset codes are below 5
    $badResets = $db->getRow("logs",array("event","result"),array("ResetpassBadCode",$_SERVER['REMOTE_ADDR']),"stamp",5,"stamp","DESC");
    if(count($badResets) < 5){
    	$pass = true;
    }else{
    	$badReset = $badResets[4]["stamp"]; // the 5th bad login, (2018-05-23 13:13:0)
    	$badTime = strtotime($badReset);
    	$valid_logs = time() - (60*30);  // for 30 minutes (60*30)
    	if($badTime > $valid_logs){
    		$pass = false;
    	}else{
    		$pass = true;
    	}
    }
    if($pass){
    	// If Total bad reset on wrong phone/account are below 5
    	$badResets = $db->getRow("logs",array("event","result"),array("ResetpassNotFound",$_SERVER['REMOTE_ADDR']),"stamp",5,"stamp","DESC");
	    if(count($badResets) < 5){
	    	$pass = true;
	    }else{
	    	$badReset = $badResets[4]["stamp"]; // the 5th bad login, (2018-05-23 13:13:0)
	    	$badTime = strtotime($badReset);
	    	$valid_logs = time() - (60*30);  // for 30 minutes (60*30)
	    	if($badTime > $valid_logs){
	    		$pass = false;
	    	}else{
	    		$pass = true;
	    	}
	    }
    }
    return  $pass;
}

// Function to make random code
function randomCode($lenght) {
	$alphabet = "abcdefghijkmnopqrstuwxyzABCDEFGHIJKMNOPQRSTUWXYZ0123456789";
	$pass = array();
	$alphaLength = strlen($alphabet) - 1;
	for ($i = 0; $i < $lenght; $i++) {
		$n = rand(0, $alphaLength);
		$pass[] = $alphabet[$n];
	}
	return implode($pass);
}

function killItWithFire(){
	$SESSION_["user_id"] = null;
	$SESSION_["user"] = null;
	$_SESSION['timestamp'] = null;
	session_unset();
	session_destroy();
}

?>
