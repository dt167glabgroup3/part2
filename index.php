<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: index.php
 * Desc: Start page for Projekt
 *
 * erho0903
 ******************************************************************************/
include "includes/start.php"; // startup file

// Dont do any post stuff unless the user is online
if(isset($_SESSION["user_id"])){

	if(isset($_POST["newpost"])){
		$uploadedMessage = uploadMessage($_POST["newpost"], $_SESSION["user_id"]);
	}
	if(isset($_POST["postIdForDelete"])){
		deleteMessage($_POST["postIdForDelete"], $_SESSION["user_id"]);
	}
	if(isset($_POST["postIdForVote"])){
		if(isset($_POST["upvote"])){
			upvoteMessage($_POST["postIdForVote"],$_SESSION["user_id"]);
		}
		elseif(isset($_POST["downvote"])){
			downvoteMessage($_POST["postIdForVote"],$_SESSION["user_id"]);
		}
	}
}

// get messages
$wordSearch = null;
$memberSearch = null;
$orderSearch = "date";
$firstSearch = "desc";
// check all GET values if there is any
if(isset($_GET["wordSearch"]) && strlen($_GET["wordSearch"]) != 0){
	$wordSearch = clean($_GET["wordSearch"]);
}
if(isset($_GET["memberSearch"]) && strlen($_GET["memberSearch"]) != 0){
	$memberSearch = clean($_GET["memberSearch"]);
}
if(isset($_GET["orderSearch"]) && $_GET["orderSearch"]=="score"){
	$orderSearch = "score";
}
if(isset($_GET["firstSearch"]) && $_GET["firstSearch"]=="asc"){
	$firstSearch = "asc";
}

$messageArray = getMessages($wordSearch,$memberSearch,$orderSearch,$firstSearch);

/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
 
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
	<?php include "includes/head.php" ?>
</head>
<body>

<?php include "includes/header.php"; ?>

<main>
	<!-- Main part for this page -->
	<div id="filterbar">
		<h1>Search</h1>
		<form method="get" action="index.php">

			<div>
				<h2>Filter</h2>
				<p>Word</p> <input type="text" name="wordSearch"<?php if(!is_null($wordSearch)){echo ' value="'.$wordSearch.'"';} ?> ><br>
				<p>Member</p> <input type="text" name="memberSearch"<?php if(!is_null($memberSearch)){echo ' value="'.$memberSearch.'"';} ?> ><br>
			</div>
			<div>
				<h2>Order</h2>
				<input type="radio" name="orderSearch" value="date"<?php if($orderSearch == "date"){echo ' checked';} ?> > date
	  			<input type="radio" name="orderSearch" value="score"<?php if($orderSearch != "date"){echo ' checked';} ?> > score<br>
				<input type="radio" name="firstSearch" value="desc"<?php if($firstSearch == "desc"){echo ' checked';} ?> > Newest/highest
	  			<input type="radio" name="firstSearch" value="asc"<?php if($firstSearch != "desc"){echo ' checked';} ?> > Lowest first<br>
	  		</div>
  			<button type="submit" name="goSearch">Go!</button>
		</form>

	</div>
	<hr>
	<div id="messageBox">
		<h1> Messages</h1>
		<?php
			if(0 == count($messageArray)){
				echo "<h2>No messages found!</h2>";
			}else{
				$db = Database_read::getInstance();
				foreach ($messageArray as $key => $post){
					echo '<div class="message">';
					echo 	'<p class="messageText">'.clean($post[0]["message"]).'<p>';

					if(isset($_SESSION["user_id"])){
						if($_SESSION["user_id"] == $post[0]["member"]){ // your post
							echo '<form class="voting" method="post" action="index.php">';
							echo '	<input class="hiddan" type="number" name="postIdForDelete" value="'.$post[0]['id'].'" readonly="readonly" />';
							echo '	<button type="submit" name="deleteMessage">DELETE</button>';
							echo '</form>';
						}else{
							$vote = $db->getRow("vote",array("member","post"),array($_SESSION["user_id"],$post[0]['id']));
							if($vote){ // has voted
								if($vote[0]["type"] == false){
									echo '<div class="voting"><p>You downvoted this message! :(</p></div>';
								}else{
									echo '<div class="voting"><p>You upvoted this message! :)</p></div>';
								}
							}else{ // has not voted
								echo '<form class="voting" method="post" action="index.php">';
								echo	'<input class="hiddan" type="number" name="postIdForVote" value="'.$post[0]["id"].'" readonly="readonly" />';
								echo	'<button type="submit" class="upvote" name="upvote">+</button>';
								echo	'<button type="submit" class="downvote" name="downvote">-</button>';
								echo '</form>';
							}
						}
					}

					$creator = $db->getRow("member","id",$post[0]["member"]);
					if($creator){
						$usersName = $creator[0]["name"];
					}else{
						$usersName = "Unknown";
					}
					echo '<p class="messageInfo">Created by: '.clean($usersName).' <br>'.$post[1].' upvotes - '.$post[2].' downvotes</p>';
					echo '</div>';
				}

			}

	?>


	</div>
	<?php if(isset($_SESSION["user"])){ ?>
	<div id="newMessageBox">
	<hr>
		<?php
			if(isset($uploadedMessage) && !$uploadedMessage){
				echo "<h2>".$uploadedMessage."</h2>";
			}
		?>
		<h1>New message</h1>
		<form method="post" action="index.php">
			<textarea name="newpost" required></textarea>
			<button type="submit" name="goSearch">Save my message!</button>
		</form>
	</div>
	<?php } ?>

</main>

<?php include "includes/footer.php"; ?>

</body>
</html>
