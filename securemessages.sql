-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 24, 2018 at 01:25 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `securemessages`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event` varchar(30) COLLATE utf8_bin NOT NULL,
  `result` varchar(256) COLLATE utf8_bin NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `event`, `result`, `stamp`) VALUES
(2, 'test', 'sss', '2018-05-23 11:21:28'),
(3, 'wrongpassword', '1', '2018-05-23 11:32:38'),
(4, 'wrongpassword', '1', '2018-05-23 11:39:07'),
(5, 'wrongpassword', '1', '2018-05-23 11:39:28'),
(9, 'wrongpassword', '1', '2018-05-23 12:07:24'),
(22, 'wrongpassword', '1', '2018-05-23 13:14:57'),
(21, 'wrongpassword', '1', '2018-05-23 13:07:01'),
(15, 'wrongpassword', '1', '2018-05-23 12:18:28'),
(16, 'wrongpassword', '1', '2018-05-23 12:18:36'),
(20, 'wrongpassword', '1', '2018-05-23 12:27:25'),
(19, 'wrongpassword', '1', '2018-05-23 12:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_bin NOT NULL,
  `password` varchar(256) COLLATE utf8_bin NOT NULL,
  `phone` int(22) NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `name`, `password`, `phone`, `stamp`) VALUES
(1, 'Test', 'asdasdasdasdasdasd', 123456, '2018-05-23 11:13:03');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member` int(10) NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `member`, `message`, `stamp`) VALUES
(5, 1, 'mess 3', '2018-05-24 11:47:14'),
(4, 2, 'Message 2', '2018-05-23 15:00:54'),
(6, 1, 'mess 4', '2018-05-24 11:54:55'),
(7, 1, 'mess 5', '2018-05-24 11:56:19'),
(9, 44, 'Test message from unknown', '2018-05-24 13:13:11');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `a` varchar(10) COLLATE utf8_bin NOT NULL,
  `b` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `c` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `a`, `b`, `c`) VALUES
(1, 'First', '2018-05-18 13:50:33', 22),
(92, 'bla', '2018-05-23 11:29:59', 55),
(93, 'bla', '2018-05-23 11:32:38', 55),
(94, 'bla', '2018-05-23 11:47:37', 55),
(95, 'bla', '2018-05-23 11:48:35', 55),
(96, 'bla', '2018-05-23 12:04:34', 55),
(97, 'bla', '2018-05-23 12:05:45', 55),
(98, 'bla', '2018-05-23 12:06:39', 55),
(99, 'bla', '2018-05-23 12:08:49', 55),
(100, 'bla', '2018-05-23 12:19:02', 55),
(101, 'bla', '2018-05-23 12:19:04', 55);

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

DROP TABLE IF EXISTS `vote`;
CREATE TABLE IF NOT EXISTS `vote` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member` int(10) NOT NULL,
  `post` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `vote`
--

INSERT INTO `vote` (`id`, `member`, `post`, `type`, `stamp`) VALUES
(8, 1, 9, 0, '2018-05-24 13:13:33'),
(5, 3, 5, 0, '2018-05-24 12:59:03'),
(7, 1, 4, 1, '2018-05-24 13:00:39');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
