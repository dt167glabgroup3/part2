<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: index.php
 * Desc: Testing classes for project.
 * Delete this before turning in assignment
 *
 * Erho0903
 ******************************************************************************/
include "includes/start.php"; // startup file

/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
	<?php include "includes/head.php" ?>
</head>
<body>

<?php include "includes/header.php"; ?>

<main>
	<!-- Main part for this page -->
	<?php 

	// Testing config class
	$config = config::getInstance();
	echo "<hr><h2>". $config->getDBAdminConnect()[0]."</h2>";
	echo "<h2>". $config->passGetMin()."</h2><hr>";


	// Testing database classes
	// the different connections
	//$db = Database_read::getInstance(); // will fail with insert and delete
	$db = Database_write::getInstance(); // will fail with read, counts and delete.
	//$db = Database_admin::getInstance(); // will work on all

	// Testing gets
	$allRows = $db->getRow("test");
	if($allRows){
		echo "<h2>Value of first:".$allRows[0]["a"]."</h2>";
	}else{
		echo "<h2>Failed to get row</h2>";
	}
	


	// Testing count
	$countRows = $db->countRow("test");
	if($countRows){
		echo "<h2>".$countRows."</h2>";
	}else{
		echo "<h2>Failed to get Count</h2>";
	}
	
	// Testing instert
	if($db->insertRow("test",array("a","c"),array("bla","55"))){
		echo "<h2>Inserted</h2>";
	}else{
		echo "<h2>Insert fail</h2>";
	}

	// Testing count again to see difference after insert
	$countRows = $db->countRow("test");
	if($countRows){
		echo "<h2>".$countRows."</h2>";
	}else{
		echo "<h2>Failed to get Count</h2>";
	}

	// Test delete
	if($db->deleteRow("test",array("a","c"),array("bla","55"))){
		echo "<h2>Deleted</h2>";
	}else{
		echo "<h2>Delete failed</h2>";
	}

	// Testing count again to see delete diff
	$countRows = $db->countRow("test");
	if($countRows){
		echo "<h2>".$countRows."</h2>";
	}else{
		echo "<h2>Failed to get Count</h2>";
	}


	//logfile("testing","adding stuff");

	




	?>

	<script>
		smsPhone("E4g2Wa","070234567");
	</script>
</main>

<?php include "includes/footer.php"; ?>

</body>
</html>
