<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: password-reset.php
 * Desc: validating
 *
 * ance
 ******************************************************************************/
include "includes/start.php"; // startup file

if(isset($_SESSION["user_id"])){
  header("Location: index.php");
  die();
}

if(!isset($_SESSION["regtime"]) && $_SESSION["regtime"] > time())
{
  header("Location: new-member.php");
  die();
}
$errormsg = "";

if(isset($_POST['validate-user']))
{
  if($_POST['regvalidation'] == $_SESSION['code'])
  {
    //Add user, log in user!
    $addmember = addNewMember($_SESSION['reguname'], $_SESSION['regphone'], $_SESSION['regpass']);
    if(!is_bool($addmember))
    {
      $errormsg = $addmember;
    }
    else
    {
      logfile("User creation", $_SESSION['reguname']);
      $newMember = getUserInfo($_SESSION['reguname']);

      $_SESSION["user"] = $newMember[0]['name'];
      $_SESSION["user_id"] = $newMember[0]['id'];
      $_SESSION['timestamp'] = time();

      unset($_SESSION['reguname']);
      unset($_SESSION['regphone']);
      unset($_SESSION['regpass']);
      unset($_SESSION['code']);

      header("Location: index.php");
    }
  }
  else
  {
      $errormsg = "Ops, check your phone again!";
  }
}
/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
	<?php include "includes/head.php" ?>
</head>
<body>

<?php include "includes/header.php"; ?>

<main>
	<!-- Main part for this page -->
  <div class="formwrapper">
    <h2>
      Register new user
    </h2>
      <form method="post">
        <label>Validation code</label>
        <input type="text" name="regvalidation" required>
        <button type="submit" name="validate-user">Validate</button>
        <?php
          if($errormsg != "")
          {
            echo '<p id="errormsg">'.$errormsg.'</p>';
          }
          echo "<script>smsPhone('".$_SESSION['code']."','".$_SESSION['regphone']."')</script>";
        ?>
      </form>
  </div>
</main>

<?php include "includes/footer.php"; ?>

</body>
</html>
